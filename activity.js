// Insert Data

db.users.insertMany([
	{
		firstName: "Jane", 
		lastName: "Doe",
		age: 21,
		contact: {
			phone: "5347786", 
			email: "rosieposie@email.com"
		}, 
		courses: ["Phyton", "React", "PHP"],
		department: "HR"
	},
	{
		firstName: "Stephen", 
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "5347786", 
			email: "ggone@email.com"
		}, 
		courses: ["React", "Laravel", "SASS"],
		department: "HR"
	}, 

	{

	firstName: "Neil", 
	lastName: "Armstrong",
	age: 82,
	contact: {
		phone: "5347786", 
		email: "jisooya@email.com"
	}, 
	courses: ["CSS", "Javascript", "Phyton"],
	department: "HR"
	}
])

// $or Operator and field projection exclusion

db.users.find({
	$or: [
	
		{
			firstName: {$regex: "s", $options: "$i"}
		},
		{
			lastName: {$regex: "d" , $options: "$i"}
		},
	]
}, 
	{
		firstName: 1, 
		lastName: 1, 
		_id:0 }
	)



// $and Operator 

db.users.find({
	$and: [
		{
			department: {$eq: "HR"}
		},
		{
			age: {$gte: 70}
		},

	]
})

// $and, $regex and $lte Operator 

db.users.find({
	$and: [
		{
			firstName: {$regex: "e"}
		},
		{
			age: {$lte: 30}
		},

	]
})
